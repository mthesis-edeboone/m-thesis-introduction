# Master Thesis Introduction

Introductory work for my Master's Internship at HEP, organised in Jupyter Notebooks and/or python scripts.

Topics:
 * [Fourier Transforms](./fourier)
 * [Antenna Simulations](./simulations)

[`lib`](./lib) contains a library.
\
Notebooks can pick it up by using
```python
import os
import sys
# Append parent directory to import path so refcurve can be found
sys.path.append(os.path.dirname(os.path.abspath(os.getcwd())))
```
