
import numpy as np

from . import sampling as smp

class Sampler():
    """
    A mechanism to sample signals.
    """

    def __init__(self, sampling_frequency=None):
        """
        Parameters
        ##########
        sampling_frequency - float
            Frequency the signals will be sampled at
        """

        self.sampling_frequency = sampling_frequency

    def sample(self, signal, signal_fs=None):
        """
        Sample signal
        """
        # Null operation
        if signal_fs is None or self.sampling_frequency is None:
            return signal

        return smp.resample(signal, signal_fs, self.sampling_frequency)
