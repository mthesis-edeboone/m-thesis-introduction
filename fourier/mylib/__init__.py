"""
Module to automatically load another (local) module.
"""

from .passband import *
from .fft import *
from .plotting import *
from .util import *
