"""
Some preconfigured ArgumentParser
"""

from argparse import ArgumentParser

def MyArgumentParser(
        default_fig_dir='./figures',
        default_show_plots=False,
        default_data_dir='./ZH_airshower',
        **kwargs):
    """
    A somewhat preconfigured ArgumentParser to be shared across
    multiple scripts.

    Set show_plots=True to by default enable showing plots.
    Likewise, set fig_dir=None to by default disable saving figures.
    """
    parser = ArgumentParser(**kwargs)

    # Whether to show plots
    group1 = parser.add_mutually_exclusive_group(required=False)
    group1.add_argument('--show-plots', action='store_true', default=default_show_plots, help='Default: %(default)s')
    group1.add_argument('--no-show-plots', dest='show-plots', action='store_false')

    # Data directory
    parser.add_argument('--data-dir', type=str, default=default_data_dir, help='Path to Data Directory. (Default: %(default)s)')

    # Figures directory
    parser.add_argument('--fig-dir', type=str, default=default_fig_dir, help='Set None to disable saving figures. (Default: %(default)s)')

    return parser
