# Airshower + Beacon simulation

Simulate receiving an airshower and beacon in an array.
Using the beacon, the clocks can be calibrated.

The ZHaires simulated airshower is stored in [./ZH_airshower](./ZH_airshower).
The produced files can be read using [./earsim](./earsim).

All quantities are stored in HDF5 files with the airshower data.

Steps:
 1. Setup
	1. Beacon ([./aa_generate_beacon.py])
		1. Define tx position
		2. Read in antennas
		3. Sample beacon at each antenna
		4. Add to relevant polarisation
		5. Save antenna traces

	2. Timeoffset ([./ab_modify_clocks.py])
		1. Generate timeoffsets for each antenna
		2. Modify time samples

 2. Beacon analysis
	1. Find beacon frequency and phase in antenna traces ([./ba_measure_beacon_phase.py])
	2. Remove phase due to distances
	3. Analyse the sigma between antenna pairs
	4. Remove 0j part and take the mean of (:,j)
	5. Assign mean as clock shift phase of antenna

 3. Find k\*2\\pi phase offsets (periods) ([./ca_periods_from_showers.py])

 4. Rewrite clocks and do interferometric reconstruction ([./da_reconstruction.py])
