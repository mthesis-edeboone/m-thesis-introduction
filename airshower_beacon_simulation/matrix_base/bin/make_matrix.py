#!/usr/bin/env python3

import os
import os.path as path
from itertools import product

baselines = [ '', 72 ]
noise_sigmas = [ 0, '1e4' ]
clock_devs = [0, 20]
trace_lengths = [ 4096 ]#, 16384 ]

for options in product(baselines, clock_devs, noise_sigmas, trace_lengths):
    baseline, clk_dev, noise, trace = options

    dirname = f"matrix_c{clk_dev}_b{baseline}_N{trace}_noise{noise}"
    print(dirname)

    # Make directory
    if path.exists(dirname):
        print(f"{dirname} already exists! continuing anyway..")

    os.makedirs(dirname, exist_ok=True)

    # Soft link clock file if available
    if True:
        os.makedirs(path.join(dirname, 'data'), exist_ok=True)

        if not path.isfile(path.join(dirname, 'data/clocks.csv')):
            os.symlink(f'../../c{clk_dev}_clocks.csv', path.join(dirname, 'data/clocks.csv'))

    # Setup config.mk
    with open(path.join(dirname, 'env.sh'), 'w') as fp:
        template = f"""
export CLK_DEV={clk_dev}
export REF_ANTS={baseline}
export NOISE_SIGMA={noise}
export TRACE_N={trace}
"""
        fp.write(template)
