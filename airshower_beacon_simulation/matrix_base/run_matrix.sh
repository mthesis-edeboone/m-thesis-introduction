#!/bin/bash

MATRIXDIR='/scratch/etdeboone/matrix'
MATRIX_ELS="${@:-$MATRIXDIR/matrix*/}"

BASE=$(realpath ../base)
MAKE="make -C ${BASE} "

for d in $MATRIX_ELS ;
do
	echo "Entering $d"
	pushd "$d";

	# Source constants
	. env.sh
	export FIG_DIR=$(realpath "./figures")
	export DATA_DIR=$(realpath "./data")

	mkdir -p $FIG_DIR $DATA_DIR

	$MAKE generate-beacon;
	$MAKE signal-to-noise;

	$MAKE clocks;

	$MAKE phases;
	#$MAKE beacon-phase;
	#$MAKE clock-phase;
	#$MAKE baseline-phase;
	#$MAKE antenna-phase;

	$MAKE findks;
	$MAKE reportks;

	#$MAKE vary-fixes;

	#$MAKE reconstruct;
	echo "Popping"
	popd
done
